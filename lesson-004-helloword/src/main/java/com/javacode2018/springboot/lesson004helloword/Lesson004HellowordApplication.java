package com.javacode2018.springboot.lesson004helloword;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson004HellowordApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson004HellowordApplication.class, args);
    }

}
