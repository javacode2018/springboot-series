package com.javacode2018.springboot.lesson004helloword;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class HelloWordController {
    @RequestMapping("/hello")
    public String hello() {
        return "你好SpringBoot!";
    }

@RequestMapping("/userList")
public List<Map<String, Object>> userList() {
    List<Map<String, Object>> list = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
        Map<String, Object> user = new HashMap<>();
        user.put("name", "路人甲Java-" + i);
        user.put("age", 30 + i);
        list.add(user);
    }
    return list;
}
}
