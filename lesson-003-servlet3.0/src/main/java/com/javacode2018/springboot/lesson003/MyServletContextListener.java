package com.javacode2018.springboot.lesson003;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebListener;

import java.util.EnumSet;

/**
 * 公众号：路人甲java，专注于java干货分享
 * 个人博客：http://itsoku.com/
 * 已推出的系列有：【spring系列】、【java高并发系列】、【MySQL系列】、【MyBatis系列】、【Maven系列】
 * git地址：https://gitee.com/javacode2018
 */
//@0：本案例中没有在web.xml中配置这个Listener，所以需要在MyServletContextListener上加上@WebListener注解，采用注解的方式来注册Listener
@WebListener
public class MyServletContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        //@1、获取ServletContext
        ServletContext sc = sce.getServletContext();

        /**
         * 注册Servlet
         */
        //@2、调用sc.addServlet注册自定义的Servlet，返回一个ServletRegistration.Dynamic对象
        ServletRegistration.Dynamic myServlet = sc.addServlet("myServlet", MyServlet.class);
        //@3、通过ServletRegistration.Dynamic对象可以设置servlet所有配置信息，这里我们只演示了2个方法，还有很多方法大家可以去试试
        myServlet.addMapping("/myServlet");
        myServlet.setInitParameter("name", "路人");
        myServlet.setInitParameter("lesson", "SpringBoot系列!");

        /**
         * 注册Filter
         */
        //@4、注册自定义的Filter，返回一个FilterRegistration.Dynamic对象
        FilterRegistration.Dynamic costTimeFilter = sc.addFilter("costTimeFilter", CostTimeFilter.class);
        //@5、通过FilterRegistration.Dynamic对象可以设置Filter的所有配置信息
        costTimeFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");

        /**
         * 注册Listener
         */
        sc.addListener(MyServletRequestListener.class);
    }
}