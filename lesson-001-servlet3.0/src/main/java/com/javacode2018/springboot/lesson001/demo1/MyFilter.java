package com.javacode2018.springboot.lesson001.demo1;

import jakarta.servlet.DispatcherType;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.annotation.WebInitParam;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * 公众号：路人甲java，专注于java干货分享
 * 个人博客：http://itsoku.com/
 * 已推出的系列有：【spring系列】、【java高并发系列】、【MySQL系列】、【MyBatis系列】、【Maven系列】
 * git地址：https://gitee.com/javacode2018
 */
@WebFilter(
        filterName = "myFilter", // filter名称
        urlPatterns = "/*", //拦截的地址
        servletNames = "myServlet", //拦截的servlet名称列表
        dispatcherTypes = {DispatcherType.REQUEST},//拦截的请求类型
        initParams = {@WebInitParam(name = "p1", value = "v1")}, //定义Filter初始化参数列表
        asyncSupported = true // 是否支持异步
)
public class MyFilter extends HttpFilter {

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        System.out.println(req.getRequestURL());
        super.doFilter(req, res, chain);
    }
}
