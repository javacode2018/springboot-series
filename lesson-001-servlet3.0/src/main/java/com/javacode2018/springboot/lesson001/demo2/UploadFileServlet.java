package com.javacode2018.springboot.lesson001.demo2;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.IOException;
import java.util.Collection;

@WebServlet(urlPatterns = "/uploadFile")
@MultipartConfig(
        location = "", //存放生成文件的地址
        maxFileSize = -1,//允许上传的文件最大值，默认为-1，表示没有限制
        maxRequestSize = -1,//针对 multipart/form-data 请求的最大数量，默认为-1，表示没有限制
        fileSizeThreshold = 1024 * 1024 * 10// 文件大小超过这个值的会被写入磁盘，默认值为0，都会写入磁盘，小于这个值的会被写在内存中
)
public class UploadFileServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //①、获取请求中的参数列表
        Collection<Part> parts = req.getParts();
        //②、遍历参数
        for (Part part : parts) {
            //③、获取文件名称，文件名称不为空的表示是文件
            String fileName = part.getSubmittedFileName();
            if (fileName != null) {
System.out.println(String.format("文件 [%s]，大小[%s byte],文件流类型：[%s]",
        fileName,
        part.getSize(),
        part.getInputStream().getClass()));

                String saveFilePath = req.getServletContext().getRealPath("/") + fileName;
                //④、调用 part.write 将上传的文件写入目标文件
                part.write(saveFilePath);
                System.out.println(String.format("文件 [%s] 上传成功，保存位置：[%s]", fileName, saveFilePath));
            } else {
                System.out.println(String.format("非文件参数：[%s->%s]", part.getName(), req.getParameter(part.getName())));
            }
        }
    }
}