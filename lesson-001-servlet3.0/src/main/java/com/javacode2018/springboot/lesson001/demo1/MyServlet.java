package com.javacode2018.springboot.lesson001.demo1;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebInitParam;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Enumeration;

/**
 * 公众号：路人甲java，专注于java干货分享
 * 个人博客：http://itsoku.com/
 * 已推出的系列有：【spring系列】、【java高并发系列】、【MySQL系列】、【MyBatis系列】、【Maven系列】
 * git地址：https://gitee.com/javacode2018
 */
@WebServlet(
        name = "myServlet", // servlet 名称
        urlPatterns = "/myServlet", // 哪些请求会被这个servlet处理
        // value 参数同 urlPatterns,二者选其一
        loadOnStartup = 1, // 设置servlet加载属性，当值为0或者大于0时，表示容器在应用启动时就加载这个servlet；当是一个负数时或者没有指定时，则指示容器在该servlet被选择时才加载；正数的值越小，启动该servlet的优先级越高。
        initParams = {
                @WebInitParam(name = "param1", value = "value1"),
                @WebInitParam(name = "param2", value = "value2")
        }, // 定义servlet初始化参数
        asyncSupported = false // 是否支持异步
)
public class MyServlet extends HttpServlet {

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Enumeration<String> initParameterNames = this.getInitParameterNames();
        while (initParameterNames.hasMoreElements()) {
            String initParamName = initParameterNames.nextElement();
            String initParamValue = this.getInitParameter(initParamName);
            resp.getWriter().append(String.format("<h1>%s:%s</h2>", initParamName, initParamValue));
        }
    }
}
