<%@ page language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <h1>servlet3.0上传多个文件</h1>
    <form enctype="multipart/form-data" method="post" action="${pageContext.request.contextPath}/uploadFile">
        <div>description:<input name="description"></div>
        <br>
        <div>file1:<input type="file" name="file1"></div>
        <br>
        <div>file2:<input type="file" name="file2"></div>
        <br>
        <div><input type="submit" value="多文件上传"></div>
    </form>
</body>
</html>
